#! /bin/bash

dir=$(dirname "$0")

usage () {
            cat << EOF
Usage: $0 [options]

	Copy required files (*.css, Makefile, ...) locally, to avoid
	depending on /usr/local/common/TEXINPUTS/ (usefull to start
	working from a workstation, and keep things compilable on
	one's laptop)

Options:
	--main FILE.tex Install template (example) file as FILE.tex.

	--help	This help message.
EOF
}

what=""
files=()
main=""
logos=(ensimag.png)

what=webpage
texmainfile=exemple.txt
texfiles=(Makefile template.txt style.css)

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
	"--main")
	    shift
	    main=$1
	    ;;
        *)
            echo "unrecognized option $1"
            usage
            exit 1
            ;;
    esac
    shift
done

if [ "$what" = "" ]; then
    echo "I don't know what to do!"
    exit 1
fi

if [ "$main" = "" ]; then
    main=$texmainfile
fi

if [ "$files" = "" ]; then
    files=("${texfiles[@]}" "${logos[@]}")
fi

cp -i "$dir"/"$texmainfile" "$main"
cp -i "${files[@]/#/$dir/}" .
perl -pi -e "s@SOURCES=exemple.txt@SOURCES=${main}@" Makefile

echo "
The following files have been copied in the current directory:
${files[@]}

You can now adapt templtate.txt and try to compile with 'make'.
"
