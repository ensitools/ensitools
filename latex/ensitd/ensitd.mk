all: $(FILE).pdf $(FILE)-corrige.pdf


LU_MASTERS=$(FILE) $(FILE)-corrige

TEXINPUTS:=./ensitd:$(TEXINPUTS)
export TEXINPUTS

include LaTeX.mk

$(FILE)-corrige.pdf: $(FILE)-corrige.tex
$(FILE)-corrige.tex:
	echo '\def\enseignant{}\input $(FILE).tex' > $(FILE)-corrige.tex
