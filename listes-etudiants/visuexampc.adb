-- programme qui permet de verifier l'attribution des PC.
--   * verif "visuelle" de l'espacement.
--   * verif automatique de l'absence de doublons.
--   * verif automatique de l'existence des postes attribues.

-- Appelé avec un argument, le script génère un CSV du type
-- machine;salle

with Ada.Text_Io, Ada.Command_Line ;
use Ada.Text_Io, Ada.Command_Line ;

with Ada.Strings.Fixed;
use  Ada.Strings.Fixed;

procedure VisuExamPC is

   --------------
   -- CONFIGURATION (A MODIFIER EVENTUELLEMENT).
   --------------

   -- descr. des machines.
   type Prefixe is (ENSIPSYS, ENSIARCHI, ENSIPC, VIDE) ;  -- VIDE = SENTINELLE
   type Suffixe is new Integer range -1..470 ;
   type Orientation is (Gauche, Droite, Haut, Bas);

   function NomPrefixe(P: Prefixe) return String is
   begin
      case P is
         when ENSIPSYS => return "ensipsys" ;
         when ENSIARCHI => return "ensiarchi" ;
         when ENSIPC => return "ensipc" ;
         when VIDE => return "" ;
      end case ;
   end ;

   function NomSuffixe(S: Suffixe) return String is
   begin
      if S < 100 then
         declare
            Res: String(1..3) ;
         begin
            Res(1):= Character'Val(S / 10 + Character'Pos('0')) ;
            Res(2):= Character'Val(S mod 10 + Character'Pos('0')) ;
            Res(3):= ' ';
            return Res ;
         end ;
      else
         return Suffixe'Image(S)(2..4);
      end if;
   end NomSuffixe;

   type Machine is record
      Pref: Prefixe ;
      Num: Suffixe ;
   end record ;

   -- descr. des salles.
   type Salle is (D200, D201, E300, E303, E301, E100, E101, E103, E200, E201, E212) ;

   PrefSalle: constant array(Salle) of Prefixe :=
     ( E300 => ENSIARCHI,
       others => ENSIPC);

   -- plus petit et plus grand numéro de poste pour chaque salles
   InfSalle: constant array(Salle) of Suffixe :=
     ( D200 => 200,
       D201 => 250,
       E300 => 2,     -- 2 et 3 sont les postes "prof"
       E303 => 300,    -- 0 et 1 sont les postes prof
       E301 => 430,
       E100 => 1,
       E101 => 16,
       E103 => 41,
       E200 => 141,
       E201 => 100,
       E212 => 350
     );

   SupSalle: constant array(Salle) of Suffixe :=
     ( D200 => 240,
       D201 => 290,
       E300 => 22,
       E303 => 338,
       E301 => 470,
       E100 => 15,
       E101 => 30,
       E103 => 80,
       E200 => 181,
       E201 => 140,
       E212 => 389
     );

   Espace: constant String(1..6) := (others => ' ') ;
   Rien: constant String(1..6) := (others => ' ') ;

   type Col_Desc is record
      Inf : Suffixe;
      N_Prem : Suffixe;
      N_Der : Suffixe;
   end record;
   type Cols_Desc is array(Suffixe range <>, Natural range <>) of
     Col_Desc;

   -- Cols_Desc(1..nombre_lignes, 1..nombre de cotes) := ...
   E101_Cols : Cols_Desc(1..3, 1..2) :=
     (1 => ((30, 1, 3), (27, 1, 2)),
      2 => ((25, 1, 3), (22, 1, 2)),
      3 => ((20, 1, 3), (17, 1, 2))
     );

   -- /!\ bons numéros de machines, mais pas bonne disposition
   E100_Cols : Cols_Desc(1..3, 1..2) :=
     (1 => ((15, 1, 3), (12, 1, 2)),
      2 => ((10, 1, 3), (17, 1, 2)),
      3 => ((5, 1, 3),  (2, 1, 2))
     );

   E103_Cols : Cols_Desc(1..9, 1..2) :=
     (1 => ((80, 1, 3), (1,  1, 0)),
      2 => ((77, 1, 3), (1,  1, 0)),
      3 => ((74, 1, 3), (71, 1, 2)),
      4 => ((69, 1, 3), (66, 1, 3)),
      5 => ((63, 1, 3), (60, 1, 3)),
      6 => ((57, 1, 3), (54, 1, 3)),
      7 => ((51, 1, 3), (48, 1, 3)),
      8 => ((45, 1, 3), (42, 1, 2)),
      9 => ((291, 1, 1), (1,  1, 0))
     );
   
   E301_Cols : Cols_Desc(1..8, 1..2) :=
     (1 => ((400, 1, 0), (441, 1, 2)),
      2 => ((437, 1, 3), (434, 1, 3)),
      3 => ((443, 1, 3), (440, 1, 3)),
      4 => ((449, 1, 3), (446, 1, 3)),
      5 => ((449, 1, 0), (452, 1, 3)),
      6 => ((458, 1, 3), (455, 1, 3)),
      7 => ((464, 1, 3), (461, 1, 3)),
      8 => ((470, 1, 3), (467, 1, 3)));

   E303_Cols : Cols_Desc(1..8, 1..2) :=
     (1 => ((305, 1, 0), (301, 1, 2)),
      2 => ((307, 1, 3), (304, 1, 3)),
      3 => ((312, 1, 2), (310, 1, 3)),
      4 => ((318, 1, 3), (315, 1, 3)),
      5 => ((324, 1, 3), (321, 1, 3)),
      6 => ((329, 1, 2), (327, 1, 3)),
      7 => ((335, 1, 3), (332, 1, 3)),
      8 => ((00, 1, -1), (338, 1, 3)));

   E200_Cols : Cols_Desc(0..7, 1..2) :=
     (0 => ((0,   0, 0), (141, 3, 3)),
      1 => ((146, 2, 3), (144, 1, 3)),
      2 => ((152, 1, 3), (149, 1, 3)),
      3 => ((158, 1, 3), (155, 1, 3)),
      4 => ((164, 1, 3), (161, 1, 3)),
      5 => ((170, 1, 3), (167, 1, 3)),
      6 => ((176, 1, 3), (173, 1, 3)),
      7 => ((181, 2, 3), (179, 1, 3))); -- ensipc125 est HS et a la place de ensipc180
   
   E201_Cols : Cols_Desc(1..8, 1..2) :=
     (1 => ((100, 1, 1), (0,   0, 0)),
      2 => ((106, 1, 3), (103, 1, 3)),
      3 => ((112, 1, 3), (109, 1, 3)),
      4 => ((118, 1, 3), (115, 1, 3)),
      5 => ((124, 1, 3), (121, 1, 3)),
      6 => ((130, 1, 3), (127, 1, 3)),
      7 => ((135, 1, 3), (132, 1, 2)),
      8 => ((140, 1, 3), (137, 1, 2)));
   
   D200_Cols : Cols_Desc(1..11, 1..1) :=
     (1 => (1 => (237, 1, 4)),
      2 => (1 => (233, 1, 4)),
      3 => (1 => (229, 1, 4)),
      4 => (1 => (225, 1, 4)),
      5 => (1 => (221, 1, 4)),
      6 => (1 => (217, 1, 4)),
      7 => (1 => (213, 1, 4)),
      8 => (1 => (209, 1, 4)),
      9 => (1 => (205, 1, 4)),
      10=> (1 => (201, 1, 4)),
      11=> (1 => (200, 1, 1)));
   
   D201_Cols : Cols_Desc(1..11, 1..1) :=
     (1 => (1 => (287, 1, 4)),
      2 => (1 => (283, 1, 4)),
      3 => (1 => (279, 1, 4)),
      4 => (1 => (275, 1, 4)),
      5 => (1 => (271, 1, 4)),
      6 => (1 => (267, 1, 4)),
      7 => (1 => (263, 1, 4)),
      8 => (1 => (259, 1, 4)),
      9 => (1 => (255, 1, 4)),
      10=> (1 => (251, 1, 4)),
      11=> (1 => (250, 1, 1)));
   
   E212_Cols : Cols_Desc(1..8, 1..2) :=
     (1 => ((384, 1, 3), (387, 1, 3)),
      2 => ((378, 1, 3), (381, 1, 3)),
      3 => ((372, 1, 3), (375, 1, 3)),
      4 => ((369, 1, 3), (0, 0, 0)),
      5 => ((363, 1, 3), (366, 1, 3)),
      6 => ((357, 1, 3), (360, 1, 3)),
      7 => ((351, 1, 3), (354, 1, 3)),
      8 => ((350, 1, 1), (0, 0, 0)));
   
   procedure Affiche(M:Machine; OrientationGD: Orientation := Gauche) ;
   -- procedure definie plus loin.

   procedure Affiche(S: Salle) is
      Pref: Prefixe := PrefSalle(S) ;
      procedure Affiche(Suf:Suffixe; OrientationGD: Orientation := Gauche) is
      begin
         Affiche((Pref,Suf), OrientationGD);
      end;

      procedure Affiche_Cols(Cols : Cols_Desc; Reversed : Boolean := False) is
         N : Suffixe;
         Sign : Suffixe;
      begin
         if Reversed then
            Sign := 1;
         else
            Sign := -1;
         end if;
         N := 1;
         -- find max nb of machines
         for C in Cols'Range loop
            for Cote in Cols'Range(2) loop
               if Cols(C, Cote).N_Der > N then
                  N := Cols(C, Cote).N_Der;
               end if;
            end loop;
         end loop;
         
         -- display
         for Cote in Cols'Range(2) loop
            for L in Suffixe range 1..N loop
               for C in Cols'Range loop
                  declare
                     Inf : Suffixe := Cols(C,Cote).Inf;
                     N_Prem : Suffixe := Suffixe(Cols(C,Cote).N_Prem);
                     N_Der : Suffixe := Suffixe(Cols(C,Cote).N_Der);
                  begin
                     --  Put(" Inf => " & Suffixe'Image(Inf) &
                     --        " Prem => " & Suffixe'Image(N_Prem) &
                     --        " Der => " & Suffixe'Image(N_Der) &
                     --        " L => " & Suffixe'Image(L));
                     if L <= N_Der and L >= N_Prem then
                        Affiche((Pref, Inf + Sign * (L-N_Prem))) ;
                     else
                        Put(Rien);
                     end if;
                     Put(Espace);
                  end;
               end loop ;
               New_Line ;
            end loop;
            New_Line ;
         end loop;
      end affiche_Cols;
   begin
      Put_Line("### Salle " & Salle'Image(S) & " -- prefixe machine = """
               & NomPrefixe(Pref) & """ ###") ;
      case S is
         when E103 =>
            Affiche_Cols(E103_Cols);
         when E101 =>
            Affiche_Cols(E101_Cols);
         when E100 =>
            Affiche_Cols(E100_Cols);
         when E301 =>
            Affiche_Cols(E301_Cols);
         when D200 =>
            Affiche_Cols(D200_Cols, True);
         when D201 =>
            Affiche_Cols(D201_Cols, True);
         when E300 =>
            Put(Espace) ;
            Put(Espace) ;
            Put(Espace) ;
            for I in Suffixe range 18..22 loop
               Affiche((Pref,I),Droite) ;
               Put(Espace) ;
            end loop ;
            New_Line ;
            for L in reverse Suffixe range 0..1 loop
               New_Line ;
               Affiche((Pref,InfSalle(S)+L)) ;  -- poste de prof
               Put(Espace) ;
               for C in Suffixe range 1..7 loop
                  Affiche((Pref,InfSalle(S)+L+C*2),Droite) ;
                  Put(Espace) ;
               end loop ;
               New_Line ;
            end loop ;
         when E303 => Affiche_Cols(E303_Cols);
            --  for L in Suffixe range 0..1 loop
            --     for C in Suffixe range 0..4 loop
            --        Affiche((Pref,SupSalle(S)-L-C*2),Droite) ;
            --        Put(Espace) ;
            --     end loop ;
            --     New_Line ;
            --  end loop ;
            --  New_Line ;
            --  for L in reverse Suffixe range 0..1 loop
            --     Affiche((Pref,InfSalle(S)+L)) ;  -- poste de prof
            --     Put(Espace) ;
            --     for C in Suffixe range 1..5 loop
            --        Affiche((Pref,InfSalle(S)+L+C*2),Droite) ;
            --        Put(Espace) ;
            --     end loop ;
            --     New_Line ;
            --  end loop ;
         when E200 => Affiche_Cols(E200_Cols);
         when E201 => Affiche_Cols(E201_Cols);
         when E212 => Affiche_Cols(E212_Cols, True);
      end case ;
   end ;

   -------
   -- ANALYSE
   -------

   SalleMach: array(Prefixe,Suffixe) of Integer := (others => (others => -1)) ;
   -- valeurs possibles des elements:
   -- -1 => non deja vue.
   -- -2 => deja vue mais inconnue.
   -- num >=0 => num de la salle correspondant a la machine.

   function EstOccupee(M: Machine) return Boolean is
   begin
      return SalleMach(M.Pref,M.Num) >= 0 ;
   end ;

   function SalleM(M: Machine) return Salle is
      -- requiert EstOccupee(M)
   begin
      return Salle'Val(SalleMach(M.Pref,M.Num)) ;
   end ;

   Occupation: array(Salle) of Boolean := (others => False) ;

   procedure AjouteMachine(M: Machine; OK: out Boolean) is
      -- met-a-jour le tableau "SalleMach"
      -- OK = True ssi la machine a une salle associee.
   begin
      for S in Salle loop
         if PrefSalle(S)=M.Pref and then M.Num in InfSalle(S)..SupSalle(S) then
            SalleMach(M.Pref,M.Num):=Salle'Pos(S) ;
            Occupation(S):=True ;
            OK:=True ;
            return ;
         end if ;
      end loop ;
      SalleMach(M.Pref,M.Num):=-2 ;
      OK:=False ;
   end ;

   Machine_Inconnue, Doublon: exception ;

   procedure AjouteLigne(M: out Machine) is
      S: String := Get_Line ;
      I: Natural := 1 ;
      OK: Boolean ;
   begin
      while I <= S'Length and then not (S(I) in '0'..'9') loop
         I:=I+1 ;
      end loop ;
      for P in Prefixe loop
         M.Pref:=P ;
         exit when S(1..I-1)=NomPrefixe(P) ;
      end loop ;
      if S'Last /= I+2 and S'Last /= I+1 then
         Put_Line(Standard_Error, "Dans machine " & S &
                    ": num de machine pas sur 2 ou 3 chiffres") ;
         raise Machine_Inconnue ;
      end if ;
      M.Num := 0 ;
      for J in I..S'Last loop
         if S(J) in '0'..'9' then
            M.Num:=M.Num*10 + Character'Pos(S(J))-Character'Pos('0') ;
         else
            Put_Line(Standard_Error, "Dans machine " & S & ": num de machine incorrect") ;
            raise Machine_Inconnue ;
         end if ;
      end loop ;
      if EstOccupee(M) then
         Put_Line(Standard_Error,"Machine " & S & ": deja occupee") ;
         raise Doublon ;
      end if ;
      AjouteMachine(M,OK) ;
      if not OK then
         if M.Pref=VIDE then
            Put_Line(Standard_Error, "Prefixe nom de machine " & S(1..I-1) & " inconnu") ;
         else
            Put_Line(Standard_Error, "Dans machine " & S & ": num de machine inconnu") ;
         end if ;
         raise Machine_Inconnue ;
      end if ;
   end ;

   -------
   -- AFFICHAGE
   -------

   procedure Affiche(M:Machine; OrientationGD: Orientation := Gauche) is
      C: Character ;
   begin
      if EstOccupee(M) then
         C:='@' ;
      else
         C:=' ' ;
      end if ;
      case OrientationGD is
         when Gauche =>
            Put(C & "]" & NomSuffixe(M.Num) & " ") ;
         when Droite =>
            Put(" " & NomSuffixe(M.Num) & "[" & C) ;
         when Haut =>
            Put(C & "U" & NomSuffixe(M.Num) & " ") ;
         when Bas =>
            Put(C & "M" & NomSuffixe(M.Num) & " ") ;
      end case;
   end ;

   procedure CSVAffiche(M:Machine) is
   begin
      Put_Line(NomPrefixe(M.Pref) & Trim(NomSuffixe(M.Num), Ada.Strings.Right) & ";" &
          Salle'Image(SalleM(M))) ;
   end ;

   procedure Legende is
   begin
      Put_Line("Chaque  ""@""  represente  un etudiant  devant un poste, dont") ;
      Put_Line("l'orientation est indiquee ""["" ou ""]"", suivi de son numero.") ;
   end ;

   procedure AfficheTouteSalles is
   begin
      Legende ;
      for S in Salle loop
         New_Line ;
         Affiche(S) ;
      end loop ;
   end ;

   procedure AfficheSallesNonVides is
   begin
      Legende ;
      for S in Salle loop
         if Occupation(S) then
            New_Line ;
            Affiche(S) ;
         end if ;
      end loop ;
   end ;

   -------
   -- MAIN
   -------
   Anomalie: exception ;
   OptionCSV: Boolean ;

   procedure AutoTeste is
      M: Machine ;
      OK: Boolean ;
   begin
      for S in Salle loop
         M.Pref:=PrefSalle(S) ;
         for I in InfSalle(S)..SupSalle(S) loop
            M.Num:=I ;
            if EstOccupee(M) then
               raise Anomalie ;
            end if ;
            AjouteMachine(M, OK) ;
            if not OK then
               raise Anomalie ;
            end if ;
            if OptionCSV then
               CSVAffiche(M) ;
            end if ;
         end loop ;
      end loop ;
   end ;

   EntreeVide: Boolean := True ;
   M: Machine ;
begin
   OptionCSV := Argument_Count >= 1 ;
   while not End_Of_File loop
      EntreeVide := False ;
      AjouteLigne(M) ;
      if OptionCSV then
         CSVAffiche(M) ;
      end if ;
   end loop ;
   if EntreeVide then
      if not OptionCSV then
         AfficheTouteSalles ;
      end if ;
      AutoTeste ;
   elsif not OptionCSV then
      AfficheSallesNonVides ;
   end if ;
end ;

