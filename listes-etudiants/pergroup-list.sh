#!/bin/bash

# Generate list.csv with a fixed room <-> group association.
# Hardcoded for CPP 1A, adapt code if needed.

USER=moy
force=f
dest=liste.csv

WGET() {
    wget --ca-certificate=intranet.ensimag.fr.cer \
	    --ask-password --user="$USER" "$@"
}

filter_out_login() {
    grep -v "^[^;]*;[^;]*;[^;]*;$1;"
}

get_group () {
    if [ ! -f liste_g"$1"_zenith.csv ] || [ "$force" = t ]; then
	WGET 'https://intranet.ensimag.fr/Zenith2/Groupe/getGroupsCoursCsv?name=cpp'"$1"'_2013' \
	    -O liste_g"$1"_zenith.csv
    fi
    cat liste_g"$1"_zenith.csv | \
	grep '^[0-9]' | \
	awk -F \; '{print $3";"$4";'"$1"';"$2";13h30-15h45;"$7}' | \
	cat > liste_g"$1".csv
}

do_group () {
    group=$1
    room=$2
    get_group "$group"
    nb_etu=$(wc -l < liste_g"$group".csv)
    ./liste-N-postes.sh --salle "$room" $nb_etu | \
	sed 's/$/;'"$room"'/' > postes_"$group".csv
    paste -d \; liste_g"$group".csv postes_"$group".csv
}

> "$dest".tmp
do_group 1a_ga E303 >> "$dest".tmp
do_group 1a_gb E301 >> "$dest".tmp
do_group 1a_gc E201 >> "$dest".tmp
do_group 1a_gd D201 >> "$dest".tmp

sort "$dest".tmp | awk -F ';' '{print $1";"$2";"$3";"$4";"$5";"$7";"$8";"$6}' > "$dest"

## List of students in 1A but not in any group. Irrelevant in
## 2013-2014, there are students from Valence in Zenith.
# get_group 1a
#
# while read line
# do
#     login=$(printf "%s" "$line" | cut -d \; -f 4)
#     if ! grep -q -e "^[^;]*;[^;]*;[^;]*;$login;" liste_g1a_g{a,b,c,d}.csv
#     then
# 	echo "$line"
#     fi
# done < liste_g1a.csv
