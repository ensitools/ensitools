Scripts utilisées pour préparer les examens de TP à l'Ensimag.
==============================================================

L'ensemble de scripts prends en entrée un fichier ensi1a.csv contenant
la liste des étudiants, et produit une répartition des étudiants sur
les machines des différentes salles, en deux sessions.

Pour les détails, lire le Makefile et ses commentaires.

Dans les grandes lignes, quand tout se passe bien :

* On édite config.sh avec les données de l'examen

* On choisit la répartition des groupes et les cas particuliers dans
  liste.sh

* On ajuste poste-session1.sh et poste-session2.sh pour génerer le bon
  nombre de postes.

* "make"

listes-etudiants fait partie du paquet `ensitools <../>`__, à
télécharger `ici <../ensitools.tar.gz>`__.


**Scripts originaux :** Sylvain Boulmé <Sylvain.Boulme@imag.fr>

**Adaptation pour l'examen Unix :** Matthieu Moy <Matthieu.Moy@imag.fr>
