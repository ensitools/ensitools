#!/bin/sh

# quick script to fill-in postes-*.txt for regular rooms.

(
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 251)); done
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 253)); done
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 254)); done
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 252)); done
echo ensipc250
) > postes-D201.txt

(
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 201)); done
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 203)); done
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 204)); done
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 202)); done
echo ensipc250
) > postes-D200.txt

(
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 45)); done
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 203)); done
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 204)); done
for i in $(seq 0 9) ; do echo ensipc$(($i * 4 + 202)); done
echo ensipc250
) > postes-E103.txt
