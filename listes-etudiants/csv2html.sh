#! /bin/sh


# 
# 

usage () {
            cat << EOF
Usage: $(basename $0) [options] CSVFILE
Options:
	--help	This help message.
EOF
}

title=
csvfile=
css=
th=no
h1=no
comment=""

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
        "--title")
	    shift
	    title=$1
            ;;
	"--h1")
	    h1=yes
	    ;;
	"--css")
	    shift
	    css=$1
	    ;;
	"--comment")
	    shift
	    comment="$1"
	    ;;
	"--th")
	    th=yes
	    ;;
        *)
	    csvfile=$1
            ;;
    esac
    shift
done

echo '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
   "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta charset="utf-8">
'
echo "<title>$title</title>"

if [ "$css" != "" ]; then
    echo '<link rel="stylesheet" type="text/css" href="'"$css"'"/>'
fi

echo "</head>
<body>"

if [ "$h1" = "yes" ]; then
    echo "<h1>$title</h1>"
fi

echo "$comment"

echo "<table>"

if [ "$th" = yes ]; then
    tag=th
else
    tag=td
fi

while read -r line; do
    echo "<tr>"
    printf "%s" "$line" | sed \
	-e "s@^@<$tag>@g" \
	-e "s@\$@</$tag>@g" \
	-e "s@;@</$tag><$tag>@g"
    echo "</tr>"
    tag=td
done <"$csvfile"

echo "</table>
</body>
</html>"
