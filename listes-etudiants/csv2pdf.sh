#! /bin/sh


usage () {
            cat << EOF
Usage: $(basename $0) [options]
Options:
	--help	This help message.
EOF
}

nopage=no
title=
comment=

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
        "--title")
	    shift
	    title=$1
            ;;
	"--comment")
	    shift
	    comment=$1
	    ;;
	"--sed")
	    shift
	    sedcmd=$1
	    ;;
	"--nopage")
	    nopage=yes
	    ;;
	"--output"|"-o")
	    shift
	    output=$(echo "$1" | sed 's/.pdf$//')
	    ;;
        *)
	    base=$(echo "$1" | sed 's/.csv$//')
            ;;
    esac
    shift
done

if [ "$base" = "" ]; then
    echo "Please, specify the CSV file name"
    usage
    exit 1
fi

if [ "$output" = "" ]; then
    output="$base"
fi

if ! command -v csv2latex; then
    echo "Can't find csv2latex. Please install it before you can continue."
    exit 1
fi

printf '%s\n' '\documentclass[a4paper]{article}

\usepackage[hmargin=1cm,vmargin=2cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{longtable}' > "$output".tex

if [ "$nopage" = "yes" ]; then
    printf '%s\n' '\pagestyle{empty}' >> "$output".tex
fi

printf '%s\n' '
\begin{document}
\centering

{\Large \bfseries '"$title"'}
\vspace{.7em}

'"$comment"'

\vspace{.7em}

\large
\renewcommand{\arraystretch}{1.22}

' >> "$output".tex

 >> "$output".tex

csv2latex --guess --nohead --lines 1000 "$base".csv | \
    sed -e "$sedcmd" \
        -e 's/{tabular}/{longtable}/' \
    >> "$output".tex || exit 1
printf '%s\n' '\end{document}' >> "$output".tex

pdflatex "$output".tex

# May be needed by longtable.
i=0
while grep -q 'Rerun LaTeX' "$output".log; do
    pdflatex "$output".tex
    i=$(($i + 1))
    if [ "$i" -eq "10" ]; then
	echo "Giving up after $i runs of LaTeX"
	exit 1
    fi
done
