#!/bin/bash

. config.sh

gnatmake genereposte.adb || exit 1
gnatmake visuexampc.adb || exit 1

aux_etu=/tmp/$(basename $0)-etu.$$
aux_machine=/tmp/$(basename $0)-machine.$$
srce_orig=ensi1a.csv
srce=/tmp/$(basename $0)-srce.$$
dest=liste.csv

rm -f ${dest}

menage() {
    rm -f "${aux_etu}" "${aux_machine}" "${srce}" "${dest}".tmp
}

erreur() {
    menage
    echo $1
    echo "Voila l'etat courant de la liste:"
    cat "${dest}"
    rm "${dest}"
    exit 1
}

verif_taille () {
    nb_etu=$(wc -l < "$1")
    nb_machines=$(wc -l < "$2")
    if [ "$nb_etu" -ne "$nb_machines" ]; then
	erreur "ERROR: $nb_etu etudiants, $nb_machines machines pour la  session $3 ..."
    else
	echo "$nb_etu etudiants pour la session $3"
    fi
}

cp ${srce_orig} ${srce}
# == CAS PARTICULIERS  A MODIFIER A LA MAIN  ICI ==
# (cf. plus bas pour les modifs à posteriori)
#grep -v -e ';fontanf$' -e ';gauthivi$' ${srce_orig} > ${srce}
# echo "Nom1;Prenom1;G6;login1;$session2-17h00;ensipsys03;D200" >> ${dest}
# echo "Nom2;Prenom2;G5;login2;$session2-17h00;ensipsys04;D200" >> ${dest}
#echo 'Florian;Fontan;G5;fontanf;9h35-11h15;ensipc25;E301' >> ${dest}
#echo 'Virginie;Gauthier;G4;gauthivi;8h15-9h35;ensipc70;E103' >> ${dest}

reformat () {
    awk -F";" '{ print $1";"$2";"$3";"$4";'"$horaire"';"$5}'
}

pivot=11

# Session 1
horaire=$session1-$session1_fin
rm -f "${aux_etu}"
# grep ';EPS;' ${srce} | reformat >> ${aux_etu}
grep G1 ${srce} | reformat >> ${aux_etu}
grep G2 ${srce} | reformat >> ${aux_etu}
grep G3 ${srce} | reformat >> ${aux_etu}
grep G4 ${srce} | reformat >> ${aux_etu}

# grep G4 ${srce} | head -n +"$pivot" | reformat >> ${aux_etu}

./poste-session1.sh | ./visuexampc csv > ${aux_machine} || erreur
verif_taille ${aux_etu} ${aux_machine} $session1
paste ${aux_etu} -d";" ${aux_machine} >> ${dest}

# Session 2
# Tiers-temps : ??
horaire=$session2-$session2_fin
rm -f "${aux_etu}"
#grep G4 ${srce} | head -n +"$pivot" | reformat >> ${aux_etu}
grep G5 ${srce} | reformat >> ${aux_etu}
grep G6 ${srce} | reformat >> ${aux_etu}
grep G7 ${srce} | reformat >> ${aux_etu}
grep G8 ${srce} | reformat >> ${aux_etu}

echo "Chaumes;Philippe;DFC;chaumesp;87000059" | reformat >> ${aux_etu}

./poste-session2.sh | ./visuexampc csv > ${aux_machine} || erreur
verif_taille ${aux_etu} ${aux_machine} $session2
paste ${aux_etu} -d";" ${aux_machine} >> ${dest}

AFFICHE_DISPO=t ./poste-session1.sh > machines-dispo.txt

# Tiers-temps
perl -pi -e 's/\(Cravic;Romain;TD_G8;.*\);9h35-10h55;\(.*\)/\1;9h35-11h15;\2/' "$dest"
#perl -pi -e 's/Florian;Fontan;G5;fontanf;9h35-11h15;ensipc25;E301/Florian;Fontan;G5;fontanf;9h35-11h15;ensipc25;E301/' "$dest"

# Modifs a posteriori (pour ne pas décaler le reste de la liste).
# (récupérer la ligne depuis liste.csv, puis corriger à la main en
# partie droite)
# perl -pi -e 's/Kevin;Castejon;G5;castejok;.*/Kevin;Castejon;G5;castejok;9h35-11h15;ensipc16;E301/' "$dest"
# perl -pi -e 's/Pierre;Berard;G4;berardpi;.*/Pierre;Berard;G4;berardpi;8h15-9h35;ensipc47;E103/' "$dest"

mv "$dest" "$dest".tmp
# Sort and put student number as last column (backward compatibility
# with previous scripts).
sort "$dest".tmp | awk -F ';' '{print $1";"$2";"$3";"$4";"$5";"$7";"$8";"$6}' > "$dest"

# effacement
menage
