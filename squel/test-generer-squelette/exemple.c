/* Un exemple presque réaliste d'exercice à trous. */

#include <stdio.h>

int fact (int x) {
        /* UNCOMMENT return fact(x); */ /* A CHANGER ! Pour éliminer un warning */
        /* START CUT */
        if (x <= 1) {
                return 1;
        } else {
                return x * fact(x - 1);
        }
        /* END CUT */
}

int main (int argc, char ** argv) {
        int x;
        scanf("%d", &x);
        printf("Fact(%d) = %d\n", x, fact(x));
        int i; /* TO DELETE */
        for (i = 0; i <= 10; i++) /* TO DELETE */
                printf("Fact(%d) = %d\n", i, fact(i)); /* TO DELETE */
        return 0;
}
