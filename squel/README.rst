generer-squelette.pl est un extracteur de squelette à partir d'un
corrigé annoté. Par exemple, si le corrigé d'un TP est ::

 int main () {
     /* START CUT */
     printf("Hello, world");
     /* END CUT */
     /* UNCOMMENT abort(); */
     return 0;
 }

alors generer-squelette.pl va automatiquement générer le squelette ::

 int main () {
     abort();
 }

(à distribuer aux étudiants)

Pour les détails, generer-squelette.pl -h et cf. le code source (et
les cas de tests dans test-generer-squelette/) !
