TEIDE-tools est un ensemble de scripts pour utiliser l'application
TEIDE utilisée à l'Ensimag pour la gestion des rendus de TPs par les
étudiants.

Exemple d'utilisation pour une correction de TPs, et envoi de retour
sur ces TPs aux étudiants :

Téléchargement de produits_42.zip sur TEIDE
===========================================

Décompression
=============

On extrait d'abord l'archive zip::


  unzip produits_42.zip
  cd produits_42/valide

Décompression des projets des étudiants
=======================================

Pour extraire toutes les archives d'un coup::

  extract-all.sh -c *

Ce script va extraire toutes les archives, il devrait marcher sur les
archives .tar, .tar.gz, .zip, .rar. L'option -c fait un nettoyage des
noms de fichiers pour éviter les problèmes avec les caractères
accentués ou autres caractères spéciaux.

Correction des TPs 
===================

(désolé, les scripts ne font pas celà à votre place ;-) ).

Pendant la correction, on peut annoter les fichiers sources
directement "dans le texte", en ajoutant des commentaires bien
identifiés. Par exemple, je fais en général ceci ::

  /* MM: Ici, vous avez fait comme ceci.
   * MM: il aurait mieux vallu faire comme cela.
   */

Pour les utilisateurs d'Emacs, le fichier moy-correction.el permet de
formatter correctement les blocs de commentaires préfixés par vos
initiales en C, C++, et assembleur.

Nettoyage des TPs des étudiants.
================================

Par exemple, en zsh::

  rm -f **/*~ **/*.o **/nom-de-lexecutable

Re-compression des TPs au format .tar.gz.
=========================================

On change le nom de l'archive (pour une archive extraite foo.tar.gz,
on crée foo-corrige.tar.gz) ::

  compress-all.sh --suffix -corrige */

Envoi automatique de mails aux étudiants
========================================

Le script batch-send-teide.sh permet d'envoyer un mail automatique à
chaque équipe, avec un corps de message contenant leur note, et leur
TP corrigé et annoté en pièce-jointe ::

  batch-send-teide.sh *-corrige.tar.gz

En fait, cette étape est un peu plus longue, il faut donner le corps
du message, le sujet du message, ... pour les détails ::

  batch-send-teide.sh --help

Commentaires
============

La dernière version de teide-tools est disponible dans l'archive
ensitools.tar.gz, disponible ici :
http://www-verimag.imag.fr/~moy/ensitools/

Pour toute remarque : Matthieu.Moy@imag.fr.
