;;; moy-correction.el --- Tools for correcting student's lab works

;; Copyright (C) 2009  Matthieu Moy

;; Author: Matthieu Moy <Matthieu.Moy@imag.fr>
;; Keywords:

;; This program is free software; you can redistribute it and/or
;; modify
;; it under the terms of the GNU General Public License as published
;; by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defun moy-guess-initials ()
  (replace-regexp-in-string "\\(.\\)[^ ]* *" "\\1" user-full-name 't))

(defconst moy-initials (moy-guess-initials)
  "Initials to use as the prefix in comments.")

;;;###autoload
(defun moy-asm-correct-mode ()
  "Make it easy to add #MM: comments in code. M-q will keep the
prefix, and C-o will start a newline with the prefix inserted."
  (interactive)
  (local-unset-key [?#])
  (setq fill-prefix (concat "# " moy-initials ": "))
  )

;; correction avec des <initiales>: en préfixe dans les commentaires.
(setq moy-c-prefix (concat "[ \\*]*\\(" moy-initials ":\\|\\)"))
(setq moy-c++-style-prefix (concat " *// *\\(" moy-initials ":\\|\\)"))
(setq moy-c++-prefix (regexp-opt (list moy-c++-style-prefix moy-c-prefix)))

(defun moy-add-to-alist (alist pair)
  (let ((newpair (assoc (car pair) (eval alist))))
    (if newpair
        (setcdr newpair (cdr pair))
      (set alist (cons pair (eval alist))))))

;;;###autoload
(defun moy-setup-c-comment-prefix ()
  "Set up `c-comment-prefix-regexp' so that M-q will work in a block
of comments prefixed by MM:."
  (interactive)
  (moy-add-to-alist 'c-comment-prefix-regexp
                   `(c-mode . ,moy-c-prefix))
  (moy-add-to-alist 'c-comment-prefix-regexp
                   `(c++-mode . ,moy-c++-prefix))
  (c-setup-paragraph-variables))

(provide 'moy-correction)
;;; moy-correction.el ends here
