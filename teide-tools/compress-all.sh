#! /bin/bash


# 
# 

usage () {
            cat << EOF
Usage: $(basename $0) [options]
Options:
	--suffix SUF	Use SUF as suffix
			compressed filename will be dirname$suffix.tar.gz
	--force,-f	Override existing file, if any.
	--zip		Create a zip file (default is a .tar.gz file)
	--verbose	Be verbose
Example:
$(basename $0) --suffix -corrige */
EOF
}

suffix=""
verbose=""
force=no
format=tgz

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
	    usage
            exit 0
            ;;
        "--suffix")
            shift
            suffix="$1"
            ;;
        "-v"|"--verbose")
            verbose=v
            ;;
        "-f"|"--force")
            force=yes
            ;;
	"--zip")
	    format=zip
	    ;;
        *)
# MM: Normaly, we compress directories, but it should work with files too.
#            if [ ! -d "$1" ]; then
#                echo "ERROR: $1 is not a directory."
#                exit 1
#            fi
            dirs=("${dirs[@]}" "$1")
            ;;
    esac
    shift
done

case "$format" in
    zip)
	extension=zip
	;;
    tgz)
	extension=tar.gz
	;;
    *)
	echo "Unknown format $format."
	exit 1
esac

for dir in "${dirs[@]}"
do
    dir=${dir%%/}
    archive="$dir$suffix.$extension"
    if [ -e "$archive" ]; then
        if [ "$force" = "yes" ]; then
            echo "WARNING: $archive already exists. Overriding ..."
        else
            echo "ERROR: $archive already exists. Use -f to override."
            continue
        fi
    fi
    echo "*** $dir -> $archive ***"
    case "$format" in
	zip)
	    if [ "$verbose" = "" ]; then
		zipopt=--quiet
	    else
		zipopt=""
	    fi
	    zip -r $zipopt "$archive" "$dir"
	    ;;
	tgz)
	    tar cz"${verbose}"f "$archive" "$dir"
	    ;;
	*)
	    echo "Unknown format $format."
	    exit 1
    esac
done
