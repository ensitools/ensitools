#! /bin/bash


usage () {
            cat << EOF
Usage: $(basename $0) [options] files
Options:
	-c, --cleanup	Cleanup file names after extraction.
	--help		This help message.
EOF
}

# Note:
# rename is part of:
# - util-linux on Linux
# - a small perl utility http://plasmasturm.org/code/rename - can be installed using "brew install rename" on homebrew
# - File::Rename in perl http://search.cpan.org/~rmbarker/File-Rename-0.20/lib/File/Rename.pm - can be installed using "sudo port install p5-file-rename" on MacPorts
for bin in rename unzip unrar 7z; do
  type $bin >/dev/null 2>&1 || { echo >&2 "I require $bin but it's not installed."; abort=1; }
done
if [ "$abort" = 1 ]; then
    echo "Aborting."
    exit 1
fi

cleanup=no

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
	    usage
            exit 0
            ;;
        "-c"|"--cleanup")
            cleanup=yes
            ;;
        *)
            archives=("${archives[@]}" "$1")
            ;;
    esac
    shift
done

[ -f extract.log ] && mv extract.log extract.log.old

printf "extraction done on %s:\n" "$(date)" > extract.log

for archive in "${archives[@]}"
do
    if [ "$archive" = "extract.log" ]; then
        echo "skipping $archive."
        continue
    fi
    if [ x"$cleanup" = x"yes" ]; then
        newname="$(echo "$archive" | perl -pe 's/[^a-zA-Z0-9\._\n-]/_/g')"
        if [ x"$newname" != x"$archive" ]; then
            mv "$archive" "$newname"
            echo "renamed $archive to $newname"
            archive="$newname"
        fi
    fi
    echo "*** $archive" | tee -a extract.log
    basename=$(echo "$archive" | sed \
        -e 's@^.*/@@' \
        -e 's/\.tar\.gz$//' \
        -e 's/\.tar\.bz2$//' \
	-e 's/\.tar.xz$//' \
        -e 's/\.tgz$//' \
        -e 's/\.zip$//' \
        -e 's/\.rar$//' \
        -e 's/\.tar$//' \
        -e 's/\.gz$//' \
	-e 's/\.7z$//')
    mkdir -p "$basename"
    (
        cd "$basename"
        echo "*** $archive" > extract.log
        case $(file -b ../"$archive") in
            "gzip"*)
                (cd .. ; cat "$archive") | (tar xzvf - || echo "ERROR extracting tar.gz archive") 2>&1 | tee -a extract.log
                ;;
            "bzip2 compressed"*)
                (cd .. ; cat "$archive") | (tar xjvf - || echo "ERROR extracting tar.bz archive") 2>&1 | tee -a extract.log
                ;;
            "POSIX tar"*|"tar "*)
                (cd .. ; cat "$archive") | (tar xvf - || echo "ERROR extracting tar archive") 2>&1 | tee -a extract.log
                ;;
            "XZ compressed data"*)
                (cd .. ; cat "$archive") | (tar xJvf - || echo "ERROR extracting tar.xz archive") 2>&1 | tee -a extract.log
                ;;
            "Zip"*)
                (cd .. ; unzip "$archive" -d "$basename" || echo "ERROR extracting zip archive") 2>&1 | tee -a extract.log
                ;;
            "RAR"*)
                (unrar x ../"$archive" || echo "ERROR extracting rar archive") 2>&1 | tee -a extract.log
                ;;
	    "7-zip archive data"*)
		(7z x ../"$archive" || echo "ERROR extracting 7z archive") 2>&1 | tee -a extract.log
		;;
            *)
                echo "ERROR: $archive: Unrecognized archive format" >> extract.log
                echo "ERROR: $(file ../$archive)" >> extract.log
                cp ../$archive .
        esac
        if [ x"$cleanup" = x"yes" ]; then
            rename 's/[^a-zA-Z0-9\._\n-]/_/g' *
        fi
    )
done

echo ""
echo "Completed"
! grep ERROR extract.log
