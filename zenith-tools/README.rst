get-ensimag-emails.sh récupère la liste des emails des étudiants
depuis l'intranet, et génère un fichier d'alias (lisible par des
mailers comme Gnus, mutt, ...)

Facilement adaptable pour (Al)pine.

Commentaires
============

La dernière version de zenith-tools est disponible dans l'archive
ensitools.tar.gz, disponible ici :
http://www-verimag.imag.fr/~moy/ensitools/

Pour toute remarque : Matthieu.Moy@imag.fr.
