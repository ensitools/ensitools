#! /bin/sh


usage () {
            cat << EOF
Usage: $(basename $0) [options]
Options:
	--help		This help message.
	--url		URL of the page listing email addresses
	--year		Year of study to fetch (like 1a, 2a, app1a, spe...).
			Automatically guesses --url and --output.
	-o, --output FILE File to write the aliases to.
	--all	        Fetch all groups

Fetches the list of email addresses from the Ensimag intranet, and
generate an alias file (usable by mailers like mutt, Gnus, ...).

Could be adapted for (Al)pine too.

EOF
}

url=""
output=""
fetch_all=""

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
	"--year")
	    shift
	    annee=$1
	    groupe=$1
	    case "$1" in
		"app"*)
		    annee=spe
		    ;;
		"spe")
		    groupe=asi_sac
		    ;;
	    esac
	    url="https://moym@intranet.ensimag.fr/Zenith2/Groupe/getGroupsCoursCsv?name=${groupe}_$(date -d '6 month ago' +'%Y')"
	    output="$HOME/.mailrc-ensimag$1"
	    ;;
        "--url")
	    shift
	    url="$1"
            ;;
	"-o"|"--output")
	    shift
	    output="$1"
	    ;;
	"--all")
	    fetch_all=yes
	    ;;
        *)
            echo "unrecognized option $1"
            usage
            exit 1
            ;;
    esac
    shift
done

run () {
    echo "\033[3;34m" "$@" "\033[0m"
    "$@"
}

if [ "$fetch_all" = "yes" ]; then
    if [ "$annee" != "" ]; then
	echo "--year and --all are not compatible"
	exit 1
    fi
    if [ "$url" != "" ]; then
	echo "--year and --url are not compatible"
	exit 1
    fi
    run "$0" --year 1a
    run "$0" --year 2a
    run "$0" --year 3a
    run "$0" --year 1aa
    run "$0" --year 2aa
    run "$0" --year 3aa
    exit 0
fi

if [ "$url" = "" ]; then
    echo "Please specify either a --url to fetch or a --year (like 1a, 2a, ...)."
    usage
    exit 1
fi

if [ "$output" = "" ]; then
    echo "Please specify a --output file."
    usage
    exit 1
fi

wget "$url" -O- | grep '^[0-9]' | \
    sed 's/^[^;]*;\([^;]*\);\([^;]*\);\([^;]*\);\([^;]*\).*/alias \1 \"\2 \3 <\4>\"/' \
    > "$output"

echo "\033[3;35m" "wrote $output ($(wc -l < $output) lines)"  "\033[0m"
echo

exit 0

# old version.

wget "$url" -O- | \
    perl -pe 's@</TR> *<TR>@</TR>\n<TR>@gi' | \
    grep -i '^<TR>.*@ensimag.imag.fr.*</TR>' > "$output"
# $1: login
# $3: prénom
# $5: nom
# $7: email
row='<TH[^>]*> *([^<]*) *(</font>)* *</TH>'
#emailrow='<TH[^>]*> <a href="mailto:([^"]*)"[^/]*/a> *</TH>'
emailrow='.*mailto:([^"]*)".*'

perl -pi -e 's@<TR>'"${row}${row}${row}${emailrow}"'@alias $1 "$3 $5 <$7>"@i' "$output"

echo "wrote $output."
