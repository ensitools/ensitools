#!/usr/bin/perl -w
 
use strict;

my $zipfile = $ARGV[0];

die "use: decompress.pl product.zip" unless defined $zipfile and $zipfile=~/\.zip/;
die "problem with product file : $!" unless -f $zipfile;

my $verbose=1;

print "unpacking main project file\n" if $verbose;
`unzip $zipfile`; 

$zipfile =~ /(\w+)\.zip/;
my $basedir = $1;

chdir($basedir);

my $file;
#put everything in current directory
if (-d "valide") {
	opendir(DIR, "valide");
	my $file;
	while( defined ($file = readdir DIR)) {
		next if $file=~ /^\.\.?$/;
		rename "valide/$file", $file;
	}
	closedir(DIR);
	rmdir("valide");
}

if (-d "non_valide") {
	opendir(DIR, "non_valide");
	while( defined ($file = readdir DIR)) {
		next if $file=~ /^\.\.?$/;
		rename "non_valide/$file", $file;
	}
	closedir(DIR);
	rmdir("non_valide");
}

print "unpacking students files\n" if $verbose;
#now decompress each student's file
my @files = <*.*>;
foreach(@files) {
	/\d+_([^_]+)_/;
	my $students_name = $1;
	mkdir($students_name);
	rename $_, "$students_name/$_";
	print "unpacking project for $students_name\n" if $verbose;
	chdir($students_name);
	decompress($_);
	chdir("..");
}

sub decompress {
	my ($file) = @_;
	my $filetype = `file "$file"`;
	if ($filetype =~/Zip archive/) {
		`unzip "$file"`;
		return;
	}
	if ($filetype =~/7-zip/) {
		`p7zip -d "$file"`;
		return;
	}
	if ($filetype =~/gzip/ or $filetype=~/Minix filesystem/) {
		#"file" sometimes identifies .gzip archives with minix filesystem ???
		`tar zxf "$file"`;
		return;
	}
	if ($filetype =~/tar archive/) {
		`tar xf "$file"`;
		return;
	}
	if ($filetype =~/bzip2/) {
		`tar jxf "$file"`;
		return;
	}
	if ($filetype =~/RAR archive/) {
		`rar x "$file"`;
		return;
	}

	print STDERR "file $file skipped : unknown file type: $filetype\n";

}
